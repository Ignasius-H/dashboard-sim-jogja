<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>FIF DC AUTODIAL JOGJA 1</title>
        <link rel="stylesheet" href="..\bootstrap\dist\css\bootstrap.css">
        <link rel="stylesheet" href="..\bootstrap\dist\css\bootstrap-grid.css">
        <link rel="stylesheet" href="..\bootstrap\dist\css\bootstrap-grid.min.css">


        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
        </link>
        <link rel="stylesheet" type="text/css"
            href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.bootstrap4.min.css">
        </link>
        <style>
            .scrollme {
                overflow-x: auto;
            }
        </style>
    </head>
    <?php
// konfigurasi database
$host     = "192.168.0.154";
$user     = "sd";
$password = "remotesd";
$database = "prod_5";
// perintah php untuk akses ke database
$koneksi = mysqli_connect($host, $user, $password, $database);

$queryM1 = mysqli_query($koneksi, "SELECT `calldate`,`phone_number`,`dstchannel`,`duration`,`disposition`,`username`,`contract_no` FROM tblCallDataRecords WHERE DATE(`calldate`)=CURDATE() GROUP BY calldate DESC LIMIT 6;");
?>

    <body>
        <!-- container -->
        <div class="container mt-3">
            <!-- row -->
            <div class="row">
                <div class="col-md-6">
                    <h3 align="center">FIF DC AUTODIAL JOGJA 1</h3>
                    <div class="table-responsive">
                        <table id="example" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>CALLDATE</th>
                                    <th>DST</th>
                                    <th>DSTCHANNEL</th>
                                    <th>DURATION</th>
                                    <th>DISPOSITION</th>
                                    <th>USERNAME</th>
                                    <th>CONTRACT.NO</th>
    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (mysqli_num_rows($queryM1) > 0) {?>
                                <?php
                                    $no = 1;
                                    while ($data = mysqli_fetch_array($queryM1)) {
                                    ?>
                                <tr>
                                    <td><?php echo $data["calldate"]; ?></td>
                                    <td><?php echo $data["phone_number"]; ?></td>
                                    <td><?php echo $data["dstchannel"]; ?></td>
                                    <td><?php echo $data["duration"]; ?></td>
                                    <td><?php echo $data["disposition"]; ?></td>
                                    <td><?php echo $data["username"]; ?></td>
                                    <td><?php echo $data["contract_no"]; ?></td>
                                </tr>
                                <?php $no++;}?>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 align="center">FIF DC AUTODIAL JOGJA 2</h3>
                    <?php
                    // konfigurasi database
                    $host     = "192.168.0.162";
                    $user     = "sd";
                    $password = "Jva[89xR";
                    $database = "prod_5";
                    // perintah php untuk akses ke database
                    $koneksi = mysqli_connect($host, $user, $password, $database);

                    $queryM1 = mysqli_query($koneksi, "SELECT `calldate`,`phone_number`,`dstchannel`,`duration`,`disposition`,`username`,`contract_no` FROM tblCallDataRecords WHERE DATE(`calldate`)=CURDATE() GROUP BY calldate DESC LIMIT 6;");
                    ?>
                    <div class="table-responsive">
                        <table id="example2" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>CALLDATE</th>
                                    <th>DST</th>
                                    <th>DSTCHANNEL</th>
                                    <th>DURATION</th>
                                    <th>DISPOSITION</th>
                                    <th>USERNAME</th>
                                    <th>CONTRACT.NO</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php if (mysqli_num_rows($queryM1) > 0) {?>
                                <?php
                                    $no = 1;
                                    while ($data = mysqli_fetch_array($queryM1)) {
                                ?>
                                <tr>
                                    <td><?php echo $data["calldate"]; ?></td>
                                    <td><?php echo $data["phone_number"]; ?></td>
                                    <td><?php echo $data["dstchannel"]; ?></td>
                                    <td><?php echo $data["duration"]; ?></td>
                                    <td><?php echo $data["disposition"]; ?></td>
                                    <td><?php echo $data["username"]; ?></td>
                                    <td><?php echo $data["contract_no"]; ?></td>
                                </tr>
                                <?php $no++;}?>
                                <?php }?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
        <hr>
        <!-- start container -->
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-6">
                    <h3 align="center">MAYBANK DC</h3>
                    <?php
                    // konfigurasi database
                    $host     = "172.16.50.10";
                    $user     = "crontab";
                    $password = "remotecrontab";
                    $database = "beyond_5_2_maybank_mirror";
                    // perintah php untuk akses ke database
                    $koneksi = mysqli_connect($host, $user, $password, $database);
    
                    $queryM1 = mysqli_query($koneksi, "SELECT `calldate`,`dst`,`src`,`lastdata`,`duration`,`disposition`,`username`,`agreementNo` FROM tblCallDataRecords WHERE DATE(`calldate`)=CURDATE() GROUP BY calldate DESC LIMIT 6;");
                    ?>
                    <div class="table-responsive">
                        <table id="example4" class="table-hover">
                            <thead>
                                <tr>
                                    <th>CALLDATE</th>
                                    <th>DST</th>
                                    <th>SRC</th>
                                    <th>LASTDATA</th>
                                    <th>DURATION</th>
                                    <th>DISPOSITION</th>
                                    <th>AGENT.ID</th>
                                    <th>AGREEMENT.NO</th>
    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (mysqli_num_rows($queryM1) > 0) {?>
                                <?php
                                $no = 1;
                                while ($data = mysqli_fetch_array($queryM1)) {
                                    ?>
                                <tr>
                                    <td><?php echo $data["calldate"]; ?></td>
                                    <td><?php echo $data["dst"]; ?></td>
                                    <td><?php echo $data["src"]; ?></td>
                                    <td><?php echo $data["lastdata"]; ?></td>
                                    <td><?php echo $data["duration"]; ?></td>
                                    <td><?php echo $data["disposition"]; ?></td>
                                    <td><?php echo $data["username"]; ?></td>
                                    <td><?php echo $data["agreementNo"]; ?></td>
    
                                </tr>
                                <?php $no++;}?>
                                <?php }?>
                            </tbody>
    
                        </table>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <h3 align="center">AWDA TELESALES MULTICAMPAIGN</h3>
                    <?php
                        // konfigurasi database
                        $host     = "192.168.0.186";
                        $user     = "crontab";
                        $password = "risetcrontab";
                        $database = "beyond_default_c2c";
                        // perintah php untuk akses ke database
                        $koneksi = mysqli_connect($host, $user, $password, $database);
    
                        $queryM1 = mysqli_query($koneksi, "SELECT `calldate`,`dst`,`dcontext`,`agentId`,`duration`,`disposition`,`noKontrak` FROM tblCallDataRecords WHERE campaign IN ('AWDA') AND DATE(`calldate`)=CURDATE() GROUP BY calldate DESC LIMIT 6;;");
                        ?>
                    <div class="table-responsive">
                        <table id="example3" class="table-hover">
                            <thead>
                                <tr>
                                    <th>CALLDATE</th>
                                    <th>DST</th>
                                    <th>DCONTEXT</th>
                                    <th>AGENT.ID</th>
                                    <th>DURATION</th>
                                    <th>DISPOSITION</th>
                                    <th>CONTRACT.NO</th>
    
    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (mysqli_num_rows($queryM1) > 0) {?>
                                <?php
                                $no = 1;
                                while ($data = mysqli_fetch_array($queryM1)) { ?>
                                <tr>
                                    <td><?php echo $data["calldate"]; ?></td>
                                    <td><?php echo $data["dst"]; ?></td>
                                    <td><?php echo $data["dcontext"]; ?></td>
                                    <td><?php echo $data["agentId"]; ?></td>
                                    <td><?php echo $data["duration"]; ?></td>
                                    <td><?php echo $data["disposition"]; ?></td>
                                    <td><?php echo $data["noKontrak"]; ?></td>
    
                                </tr>
                                <?php $no++;}?>
                                <?php }?>
                            </tbody>
    
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->

        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/datatables.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js">
        </script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js">
        </script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.bootstrap4.min.js">
        </script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.colVis.min.js"></script>


        <script type="text/javascript">
            $(document).ready(function () {
                $(document).ready(function () {
                    let table = $('#example').DataTable({
                        paging: false,
                        buttons: false,
                        searching: false
                        //buttons: [ 'copy']
                    });

                    table.buttons().container()
                        .appendTo('#example_wrapper .col-md-6:eq(0)');

                    let table4 = $('#example4').DataTable({
                        paging: false,
                        buttons: false,
                        searching: false
                        //buttons: [ 'copy']
                    });

                    table4.buttons().container()
                        .appendTo('#example_wrapper4 .col-md-6:eq(0)');

                    let table3 = $('#example3').DataTable({
                        paging: false,
                        buttons: false,
                        searching: false
                        //buttons: [ 'copy']
                    });

                    table3.buttons().container()
                        .appendTo('#example_wrapper3 .col-md-6:eq(0)');

                    let table2 = $('#example2').DataTable(
                    {
                        paging:false,
                        buttons: false,
                        searching: false
                        //buttons: [ 'copy']
                    } );

                    table2.buttons().container()
                        .appendTo( '#example_wrapper2 .col-md-6:eq(0)' );
                });
            });
        </script>

    </body>

</html>