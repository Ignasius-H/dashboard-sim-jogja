<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DATA CALLBACK</title>
		<link rel="stylesheet" href="..\bootstrap\dist\css\bootstrap.css">
		<link rel="stylesheet" href="..\bootstrap\dist\css\bootstrap-grid.css">
		<link rel="stylesheet" href="..\bootstrap\dist\css\bootstrap-grid.min.css">

	
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"></link>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.bootstrap4.min.css"></link>
	
</head>	
			<?php
			// konfigurasi database
			$host       =   "172.16.50.10";
			$user       =   "crontab";
			$password   =   "remotecrontab";
			$database   =   "beyond_5_2_maybank_mirror"; //MaybankMirror
			// perintah php untuk akses ke database
			$koneksi = mysqli_connect($host, $user, $password, $database);

			$queryM1 = mysqli_query($koneksi,"SELECT tblUsers.`fullName`,tblUsers.userName,tblCustomerId.`timeStart`,tblCustomerId.`noKontrak`,tblCustomerId.taskStatus,tblCustomerId.`jamCallBack`
FROM tblCustomerId INNER JOIN tblUsers ON tblCustomerId.markedBy = tblUsers.id
WHERE taskStatus IN('NOAN','NO ACTIVE','PTP','BPH','PTP','PTD-D','CALLBACK','PAID');");
			?>

<body>
</br>

<h3 align="center">UNCONTACTED MAYBANK</h3><hr>
<div class="container">
    <div class="row">
		<div class="col-sm mt-3">
			<table id="example" class="display">
					<thead>
						<tr>
							<th>NAMA</th>
							<th>ID</th>
							<th>NO.KONTRAK</th>
							<th>TASK.STATUS</th>
							<th>JAM.CALLBACK</th>
						</tr>
					</thead>
				   <tbody>
							<?php if(mysqli_num_rows($queryM1)>0){ ?>
							<?php
								$no = 1;
								while($data = mysqli_fetch_array($queryM1)){
							?>
					<tr>
						<td><?php echo $data["fullName"];?></td>
						<td><?php echo $data["userName"];?></td>
						<td><?php echo $data["noKontrak"];?></td>
						<td><?php echo $data["taskStatus"];?></td>
						<td><?php echo $data["jamCallBack"];?></td>
						
					</tr>
							<?php $no++; } ?>
							<?php } ?>
				</tbody>

			</table>
		</div>
	</div>
</div>

	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/datatables.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.bootstrap4.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.colVis.min.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
			$(document).ready(function() {
				var table = $('#example').DataTable( {
					paging:false,
					buttons: false
					//buttons: [ 'copy']
				} );
			 
				table.buttons().container()
					.appendTo( '#example_wrapper .col-md-6:eq(0)' );
			} );
		} );
	</script>
	
</body>
</html>